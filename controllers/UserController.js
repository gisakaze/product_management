//Import the dependencies
const hashPassword = require('../utils/hash')
const _ = require('lodash')
const express = require('express');
const {
    User,
    validate
} = require('../models/user.model')

//Creating a Router
var router = express.Router();

//get all users
router.get('/', (req, res) => {
    User.find()
        .then(users => res.status(200).send(users))
        .catch(err => res.send(err))
})

//get all admins
router.get('/admins', (req, res) => {
    User.find({
            isAdmin: "true"
        })
        .then(users => res.send(users))
        .catch(err => res.send(err).status(400))
});

//get all non admins
router.get('/notAdmins', (req, res) => {
    User.find({
            isAdmin: "false"
        })
        .then(users => res.send(users))
        .catch(err => res.status(404).send(err))
});

//delete a user
router.delete('/delete/:id', (req, res) => {
    User.findByIdAndDelete({
            _id: req.params.id
        })
        .then(users => res.status(200).send(users))
        .catch(err => res.status(404).send(err))
});

//users sign up
router.post('/', async(req, res) => {
    const {
        error
    } = validate(req.body)
    if (error) return res.send(error.details[0].message).status(400)
        //check if user does not exist
    let user = await User.findOne({
        email: req.body.email
    })
    if (user) return res.send('User already registered').status(400)
        //else reggister
    user = new User(_.pick(req.body, ['name', 'email', 'password', 'isAdmin']))
    const hashed = await hashPassword(user.password)
    user.password = hashed
    await user.save()

    return res.send(_.pick(user, ['_id', 'name', 'email', 'isAdmin'])).status(201)
});

module.exports = router;