//Import the dependencies
const mongoose = require('mongoose')
const express = require('express')
const Category = mongoose.model("Categories")
const admin = require('../middlewares/isAdmin')
const _ = require("lodash")
    //Creating a Router
var router = express.Router();

//get all categories
router.get('/', (req, res) => {
    Category.find()
        .then(cateogries => res.send(cateogries))
        .catch(err => res.send(err).status(404))
});

//get one by id
router.get('/:id', async(req, res) => {
    await Category.find({
            _id: req.params.id
        })
        .then(cateogries => res.send(cateogries).status(200))
        .catch(err => res.status(404).send(err))
})

//update
router.put('/:id', admin, async(req, res) => {
    if (!Category.findById({
            _id: req.params.id
        })) return res.status(400).send("category not found");
    await Category.findOneAndUpdate({
            _id: req.params.id
        }, req.body, {
            new: true
        })
        .then(category => res.send(category))
        .catch(err => res.send(err).status(400));
})

//delete
router.delete('/:id', admin, async(req, res) => {
    await Category.findByIdAndDelete({
            _id: req.params.id
        })
        .then(category => res.send(category).status(200))
        .catch(err => res.status(400).send(err))
})

//insert new category
router.post('/', admin, (req, res) => {
    let Cat = new Category(_.pick(req.body, ['name', 'description']))
    Cat.save()
    return res.send(_.pick(Cat, ['_id', 'name', 'description'])).status(201)
});

module.exports = router;