const mongoose = require('mongoose');

//Attributes of the Course object
var categorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        minlength: 3,
        maxlength: 400
    }
});

mongoose.model('Categories', categorySchema);