const mongoose = require('mongoose');
const Joi = require('joi')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const config = require('config')
    //Attributes of the product object
var productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 255,
        minlength: 3
    },
    price: {
        type: String,
        required: true,
    },
    categoryId: {
        type: String,
        required: true,
        maxlength: 1024,
        minlength: 1
    }
});
mongoose.model('Products', productSchema);