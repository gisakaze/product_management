const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/Shopping', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('connected to mongodb successfully....'))
    .catch(err => console.log('failed to connect to mongodb', err));

//Connecting Node and MongoDB
require('./category.model');
require('./product.model');
require('./user.model');